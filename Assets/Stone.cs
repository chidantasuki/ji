using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stone : MonoBehaviour
{
    GameObject[] floor;
    GameObject[] rope;
    void Awake()
    {
        floor = GameObject.FindGameObjectsWithTag("Trigger");
        rope = GameObject.FindGameObjectsWithTag("Rope");
    }

    
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Trigger")
        { 
            foreach(var item in floor)
            {
                item.GetComponent<Floor>().start = true;
            }
            foreach(var item in rope)
            {
                item.GetComponent<Rope>().start = true;
            }
        }
    }
}
