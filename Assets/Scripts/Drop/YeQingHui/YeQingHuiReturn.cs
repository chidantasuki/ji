using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YeQingHuiReturn : MonoBehaviour
{
    public float gravity;
    public Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(0, -gravity, 0);
    }
    void Update()
    {

        if (gameObject.transform.position.y <= -10)
            YeQingHuiDropPool.instance.ReturnPool(gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("test") && gameObject.transform.position.y >= 80)
            YeQingHuiDropPool.instance.ReturnPool(gameObject);
        if (other.gameObject.CompareTag("Player"))
            YeQingHuiDropPool.instance.ReturnPool(gameObject);

    }

}
