using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSize : MonoBehaviour
{
    //摄像机
    public GameObject Camera;

    void Update()
    {
        //计算高宽比
        float x = (float)Screen.height / (float)Screen.width;

        //计算合适的摄像机距离
        float y = 2.56f * x + 0.47f;

        //相机距离自适应
        Camera.GetComponent<Camera>().orthographicSize = y;
    }
}
