using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaoMove : MonoBehaviour
{
    public float updownSpeed;
    public float speed;
    private float Speed;
    public float changeFrequence;
    public Rigidbody rb;
    public GameObject showLao;
    int time;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Speed = RandomDirection() * speed;
        rb.velocity = new Vector3(Speed, -updownSpeed, 0);
    }


    void Update()
    {
        time++;
        MoveMent();
        if (gameObject.transform.position.x <= 30 && gameObject.transform.position.x >= -30
            && gameObject.transform.position.y <= 22 && gameObject.transform.position.y >= 6)
        {
            if (time % changeFrequence == 0)
            {
                int a = Random.Range(0, 4);
                switch (a)
                {
                    case 0:
                        rb.velocity = new Vector3(-rb.velocity.x, -rb.velocity.y, 0);
                        break;
                    case 1:
                        rb.velocity = new Vector3(rb.velocity.x, -rb.velocity.y, 0);
                        break;
                    case 2:
                        rb.velocity = new Vector3(-rb.velocity.x, rb.velocity.y, 0);
                        break;
                    case 3:
                        rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, 0);
                        break;
                    default: break;
                }

            }
        }
    }
    public void MoveMent()
    {
        if (Speed >= 0)
        {
            if (gameObject.transform.position.x >= 25)
                rb.velocity = new Vector3(-Speed, rb.velocity.y, 0);
            if (gameObject.transform.position.x <= -25)
                rb.velocity = new Vector3(Speed, rb.velocity.y, 0);
        }
        else if (Speed < 0)
        {
            if (gameObject.transform.position.x >= 25)
                rb.velocity = new Vector3(Speed, rb.velocity.y, 0);
            if (gameObject.transform.position.x <= -25)
                rb.velocity = new Vector3(-Speed, rb.velocity.y, 0);
        }
        if (gameObject.transform.position.y >= 25)
            rb.velocity = new Vector3(rb.velocity.x, -updownSpeed, 0);
        else if (gameObject.transform.position.y <= 3)
            rb.velocity = new Vector3(rb.velocity.x, updownSpeed, 0);
    }
    public int RandomDirection()
    {
        int a = Random.Range(-4, 4);
        int b = 1;
        if (a >= 0)
            b = 1;
        if (a < 0)
            b = -1;
        return b;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            showLao.SetActive(true);
            oldCreateManager.Lao = false;
            gameObject.SetActive(false);
        }
    }
}
