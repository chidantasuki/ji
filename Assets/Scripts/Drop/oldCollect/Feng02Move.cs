using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Feng02Move : MonoBehaviour
{
    public float updownSpeed;
    public float speed;
    private float Speed;
    public float changeFrequence;
    public Rigidbody rb;
    public GameObject showFeng02;
    int time;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Speed = RandomDirection() * speed;
        rb.velocity = new Vector3(Speed, -updownSpeed, 0);
    }


    void Update()
    {
        time++;
        MoveMent();
        if (time % changeFrequence == 0)
            TransChange();
    }
    public void MoveMent()
    {
        if (Speed >= 0)
        {
            if (gameObject.transform.position.x >= 25)
                rb.velocity = new Vector3(-Speed, rb.velocity.y, 0);
            if (gameObject.transform.position.x <= -25)
                rb.velocity = new Vector3(Speed, rb.velocity.y, 0);
        }
        else if (Speed < 0)
        {
            if (gameObject.transform.position.x >= 25)
                rb.velocity = new Vector3(Speed, rb.velocity.y, 0);
            if (gameObject.transform.position.x <= -25)
                rb.velocity = new Vector3(-Speed, rb.velocity.y, 0);
        }
        if (gameObject.transform.position.y >= 25)
            rb.velocity = new Vector3(rb.velocity.x, -updownSpeed, 0);
        else if (gameObject.transform.position.y <= 3)
            rb.velocity = new Vector3(rb.velocity.x, updownSpeed, 0);
    }
    public int RandomDirection()
    {
        int a = Random.Range(-4, 4);
        int b = 1;
        if (a >= 0)
            b = 1;
        if (a < 0)
            b = -1;
        return b;
    }
    public void TransChange()
    {
        int a, b;
        a = Random.Range(-3, 4);
        b = Random.Range(2, 6);
        if (gameObject.transform.position.x <= 30 && gameObject.transform.position.x >= -30
            && gameObject.transform.position.y <= 20 && gameObject.transform.position.y >= 8)
            gameObject.transform.position = new Vector3(a * Random.Range(1, 9), b * Random.Range(1, 4), 0);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            showFeng02.SetActive(true);
            oldCreateManager.Feng02 = false;
            gameObject.SetActive(false);
        }
    }
}
