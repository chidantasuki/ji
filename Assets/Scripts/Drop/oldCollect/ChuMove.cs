using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChuMove : MonoBehaviour
{
    public float updownSpeed;
    public float speed;
    private float Speed;
    public Rigidbody rb;
    public GameObject showChu;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Speed = RandomDirection() * speed;
        rb.velocity = new Vector3(Speed, -updownSpeed, 0);
    }


    void Update()
    {
        MoveMent();
    }
    public void MoveMent()
    {
        if (Speed >= 0)
        {
            if (gameObject.transform.position.x >= 25)
                rb.velocity = new Vector3(-Speed, rb.velocity.y, 0);
            if (gameObject.transform.position.x <= -25)
                rb.velocity = new Vector3(Speed, rb.velocity.y, 0);
        }
        else if (Speed < 0)
        {
            if (gameObject.transform.position.x >= 25)
                rb.velocity = new Vector3(Speed, rb.velocity.y, 0);
            if (gameObject.transform.position.x <= -25)
                rb.velocity = new Vector3(-Speed, rb.velocity.y, 0);
        }
        if (gameObject.transform.position.y >= 25)
            rb.velocity = new Vector3(rb.velocity.x, -updownSpeed, 0);
        else if (gameObject.transform.position.y <= 3)
            rb.velocity = new Vector3(rb.velocity.x, updownSpeed, 0);
    }
    public int RandomDirection()
    {
        int a = Random.Range(-4, 4);
        int b = 1;
        if (a >= 0)
            b = 1;
        if (a < 0)
            b = -1;
        return b;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            showChu.SetActive(true);
            oldCreateManager.Chu = false;
            gameObject.SetActive(false);
        }
    }
}
