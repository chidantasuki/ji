using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AoLiGeiReturn : MonoBehaviour
{
    public float gravity;
    public Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(0, -gravity, 0);
    }
    void Update()
    {

        if (gameObject.transform.position.y <= -10)
            AoLiGeiDroopPool.instance.ReturnPool(gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("test") && gameObject.transform.position.y >= 80)
            AoLiGeiDroopPool.instance.ReturnPool(gameObject);
        if (other.gameObject.CompareTag("Player"))
            AoLiGeiDroopPool.instance.ReturnPool(gameObject);

    }
}
