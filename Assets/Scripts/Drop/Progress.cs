using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Progress : MonoBehaviour
{
    public static bool lv1=true;
    public static bool lv2;
    public static bool lv3;
    public int Lv1toLv2Num;
    public int Lv2toLv3Num;
    void Update()
    {
        if (PlayerMove.num >= Lv1toLv2Num)
        {
            lv1 = false;
            lv2 = true; 
        }
        if (PlayerMove.num >= Lv2toLv3Num)
        {
            lv2 = false;
            lv3 = true;
        }
    }
}
