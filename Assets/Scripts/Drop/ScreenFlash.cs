using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenFlash : MonoBehaviour
{
    public Image img;
    public Color FlashColor;
    private Color originColor;
    public float time;   
    
    void Start()
    {
        originColor = img.color;
    }

    void Update()
    {
        
    }
    public void FlashScreen()
    {
        StartCoroutine(Flash());
    }
    IEnumerator Flash()
    {
        img.color = FlashColor;
        yield return new WaitForSeconds(time);
        img.color = originColor;
    }
}
