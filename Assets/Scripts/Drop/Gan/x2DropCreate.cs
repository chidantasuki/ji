using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class x2DropCreate : MonoBehaviour
{
    public int DropMaxNum;
    public float lv1frequency;
    public float lv1frequencyCreate;
    public float lv2frequency;
    public float lv2frequencyCreate;
    public float lv3frequency;
    public float lv3frequencyCreate;

    int time;

    void Update()
    {
        time++;
        if (Progress.lv1)
        {
            if (time % lv1frequency == 0)
                CreateDrop();
        }else if (Progress.lv2)
        {
            if (time % lv2frequency == 0)
                CreateDrop();
        }else if (Progress.lv3)
        {
            if (time % lv3frequency == 0)
                CreateDrop();
        }

    }
    public Vector3 RandomPlace()
    {
        int a = Random.Range(-3, 4);
        Vector3 place = new Vector3(a * Random.Range(1, 9), 50, 0);
        return place;
    }
    public void CreateDrop()
    {
        int a = Random.Range(0, DropMaxNum);
        for (int i = 0; i <= a - 1; i++)
        {
            if(Progress.lv1)
                StartCoroutine(InstantiateDrop(lv1frequencyCreate));
            else if(Progress.lv2)
                StartCoroutine(InstantiateDrop(lv2frequencyCreate));
            else if(Progress.lv3)
                StartCoroutine(InstantiateDrop(lv3frequencyCreate));
        }
    }
    public void InstantiateDrop()
    {
        x2DropPool.instance.GetFromPool().transform.position = RandomPlace();
    }
    IEnumerator InstantiateDrop(float t)
    {
        yield return new WaitForSeconds(t);
        x2DropPool.instance.GetFromPool().transform.position = RandomPlace();

    }
}
