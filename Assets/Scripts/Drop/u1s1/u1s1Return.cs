using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class u1s1Return : MonoBehaviour
{
    public float gravity;
    public Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(0, -gravity, 0);
    }
    void Update()
    {

        if (gameObject.transform.position.y <= -10)
            u1s1DropPool.instance.ReturnPool(gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("test") && gameObject.transform.position.y >= 80)
            u1s1DropPool.instance.ReturnPool(gameObject);
        if (other.gameObject.CompareTag("Player"))
            u1s1DropPool.instance.ReturnPool(gameObject);

    }

}
