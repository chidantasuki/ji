using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMove : MonoBehaviour
{
    public Rigidbody rb;
    public float speed;
    bool isMove;
    public static int num;
    public float health;
    private float healthMax;
    public Image healthSlider;
    private ScreenFlash sf;
    public GameObject p;
    public GameObject p2;
    public GameObject p3;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        healthMax = health;
        sf = GetComponent<ScreenFlash>();
    }

    void Update()
    {
        if (health <= 0)
        { 
            Destroy(gameObject);
            SoundManager.DieAudio();
            p2.SetActive(true);
            Time.timeScale = 0;

        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            p.SetActive(true);
            Time.timeScale = 0;
        }
        if (oldCreateManager.isFinish)
        {
            p3.SetActive(true);
            SoundManager.FinishAudioPlay();
            Time.timeScale = 0;
        }
    }
    private void FixedUpdate()
    {
        LRMove();
    }
    public void HpCtrl()
    {
        healthSlider.fillAmount = health/healthMax;
    }
    public void LRMove()
    {
        float HorizontalMove = Input.GetAxisRaw("Horizontal");
        rb.velocity = new Vector3(HorizontalMove * speed, rb.velocity.y,0);
        if (HorizontalMove != 0)
        {
            transform.localScale = new Vector3(HorizontalMove*1, 1, 1);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("old"))
        { 
            num++;
            SoundManager.GetAudioPlay();
        }
        if (other.gameObject.CompareTag("test"))
        { 
            health--;
            SoundManager.HurtAudioPlay();
            sf.FlashScreen();
            HpCtrl();
        }
    }
}
