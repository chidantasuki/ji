using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;
    AudioSource BkSource;
    AudioSource PlayerSource;
    [SerializeField]
    private AudioClip bgmAudio;
    [SerializeField]
    private AudioClip getAudio;
    [SerializeField]
    private AudioClip hurtAudio;
    [SerializeField]
    private AudioClip finishAudio;
    [SerializeField]
    private AudioClip dieAudio;
    private void Awake()
    {
        instance = this;
        BkSource = gameObject.AddComponent<AudioSource>();
        PlayerSource = gameObject.AddComponent<AudioSource>();
    }
    void Start()
    {
        BGMAudioPlay();
    }

    void Update()
    {
        
    }
    public static  void BGMPause()
    {
        instance.BkSource.Pause();
    }
    public static void BGMAudioPlay()
    {
        instance.BkSource.clip = instance.bgmAudio;
        instance.BkSource.loop = true;
        instance.BkSource.Play();
    }
    public static void GetAudioPlay()
    {
        instance.PlayerSource.clip = instance.getAudio;
        instance.PlayerSource.Play();
    }
    public static void HurtAudioPlay()
    {
        instance.PlayerSource.clip = instance.hurtAudio;
        instance.PlayerSource.Play();
    }
    public static void FinishAudioPlay()
    {
        instance.BkSource.clip = instance.finishAudio;
        instance.BkSource.loop = true;
        instance.BkSource.Play();
    }
    public static void DieAudio()
    {
        instance.PlayerSource.clip = instance.dieAudio;
        instance.PlayerSource.Play();
    } 
}
