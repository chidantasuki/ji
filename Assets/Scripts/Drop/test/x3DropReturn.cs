using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class x3DropReturn : MonoBehaviour
{
    public float gravity;
    public float speed;
    private float Speed;
    public Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        Speed = RandomDirection() * speed;
        rb.velocity = new Vector3(Speed, -gravity, 0);
    }  
    void Update()
    {
        MoveMent();
        if (gameObject.transform.position.y <= -10)
            x3DropPool.instance.ReturnPool(gameObject);
    }
    public void MoveMent()
    {
        if (Speed >= 0)
        {
            if (gameObject.transform.position.x >= 42)
                rb.velocity = new Vector3(-Speed, -gravity, 0);
            if (gameObject.transform.position.x <= -42)
                rb.velocity = new Vector3(Speed, -gravity, 0);
        }
        else
        {
            if (gameObject.transform.position.x >= 42)
                rb.velocity = new Vector3(Speed, -gravity, 0);
            if (gameObject.transform.position.x <= -42)
                rb.velocity = new Vector3(-Speed, -gravity, 0);
        }
    }
    public int RandomDirection()
    {
        int a = Random.Range(-4, 4);
        int b=1;
        if (a >= 0)
            b = 1;
        if (a < 0)
            b = -1;
        return b;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
            x3DropPool.instance.ReturnPool(gameObject);
    }
}
