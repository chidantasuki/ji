using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gkdDropPool : MonoBehaviour
{
    public static gkdDropPool instance;

    public GameObject x1dropPrefeb;
    public int dropMaxCount;

    private Queue<GameObject> availableObjects = new Queue<GameObject>();

    private void Awake()
    {
        instance = this;

        FillPool();
    }

    public void FillPool()
    {
        for (int i = 0; i < dropMaxCount; i++)
        {
            var newDrop = Instantiate(x1dropPrefeb);
            newDrop.transform.SetParent(transform);

            ReturnPool(newDrop);
        }
    }
    public void ReturnPool(GameObject gameObject)
    {
        gameObject.SetActive(false);
        availableObjects.Enqueue(gameObject);
    }
    public GameObject GetFromPool()
    {
        if (availableObjects.Count == 0)
        {
            FillPool();
        }
        var outDrop = availableObjects.Dequeue();
        outDrop.SetActive(true);
        return outDrop;
    }


}
