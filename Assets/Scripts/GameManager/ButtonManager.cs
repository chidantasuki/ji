using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class ButtonManager : MonoBehaviour
{
    [Header("汉字部分")]
    public GameObject wood;
    public GameObject stone;
    public GameObject water;
    public GameObject tree;
    [Header("控制参数")]
    public int chooseNum;
    public int woodNum;
    public int stoneNum;
    public int waterNum;

    public GameObject player;

    public GameObject shuig;
    public GameObject baig;
    public GameObject shi1;
    public GameObject shi2;
    public GameObject mu;
    public GameObject p;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void wood1()
    {
        if (chooseNum < 2)
        {
            woodNum++;
            chooseNum++;
            mu.SetActive(true);
        }
    }
    public void bai()
    {
        if (chooseNum < 2)
        {
            woodNum++;
            waterNum++;
            chooseNum++;
            baig.SetActive(true);
        }
        
    }
    public void shui()
    {
        if (chooseNum < 2)
        {
            waterNum++;
            chooseNum++;
            shuig.SetActive(true);
        }
    }
    public void stone1()
    {
        if (chooseNum < 2)
        {
            stoneNum++;
            chooseNum++;
            shi1.SetActive(true);
        }
    }
    public void stone2()
    {
        if (chooseNum < 2)
        {
            stoneNum++;
            chooseNum++;
            shi2.SetActive(true);
        }
    }
    public void instantiateObj()
    {
        if (woodNum == 2)
            Instantiate(wood, player.transform.position + new Vector3(0, 2, 0), Quaternion.identity);
        else if (stoneNum == 2)
            Instantiate(stone, player.transform.position+new Vector3(0,2,0), Quaternion.identity);
        else if (waterNum == 2)
            water.SetActive(true);
        woodNum = 0;
        stoneNum = 0;
        waterNum = 0;
        chooseNum = 0;
        mu.SetActive(false);
        shi1.SetActive(false);
        shi2.SetActive(false);
        baig.SetActive(false);
        shuig.SetActive(false);
    }
    public void Continue()
    {
        Time.timeScale = 1;
        p.SetActive(false);
    }
    public void back()
    {
        SceneManager.LoadScene("SampleScene");
        Time.timeScale = 1;
    }
    public void quit()
    {
        Application.Quit();
    }
    public void Return3()
    {
        SceneManager.LoadScene("Drop");
        Time.timeScale = 1;
    }
}
