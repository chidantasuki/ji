using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    public int speed;
    Vector3 moveDir;
    Vector3 realMoveDir;
    private Rigidbody rb;
    public Transform trs;
    public bool onBoat=false;
    public GameObject p;
    public GameObject P2;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

        if (onBoat)
            rb.useGravity = false;
        if (!onBoat)
            rb.useGravity = true;
        Move();
        if (Input.GetKeyDown(KeyCode.Escape))
            p.SetActive(true);
    }




    private void Move()
    {
        moveDir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        Ray ray = new Ray(trs.position, Vector3.down);
        RaycastHit hit;
        Vector3 hitNormal;
        Physics.Raycast(ray, out hit, 1f);
        hitNormal = hit.normal.normalized;
        realMoveDir = Vector3.ProjectOnPlane(moveDir, hitNormal).normalized;

        rb.MovePosition(transform.position + realMoveDir * speed * Time.fixedDeltaTime);
        

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "EndGame")
            P2.SetActive(true);
    }
}
