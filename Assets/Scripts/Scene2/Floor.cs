using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
    public bool up;
    public Transform target;
    public float speed;
    Vector3 dir;
    public bool start = false;   
    void Start()
    {
        dir = target.position;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (up&&start)
            if (transform.position.y < dir.y)
                transform.position += new Vector3(0, 1, 0) * speed * Time.deltaTime;
        if (!up&&start)
            if (transform.position.y > dir.y)
                transform.position -= new Vector3(0, 1, 0) * speed * Time.deltaTime;
    }
}
