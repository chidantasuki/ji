using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public Transform player;
    Vector3 differ;
    void Start()
    {
        differ = transform.position - player.position;    
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = player.position + differ;
    }
}
