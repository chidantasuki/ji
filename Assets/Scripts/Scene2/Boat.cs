using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boat : MonoBehaviour
{
    public move Move;
    public GameObject player;
    public bool ifboat=false;
    Vector3 dir;
    private Animator animator;
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!ifboat)
            return;
        if (ifboat)
        {
            transform.position = player.transform.position + dir;
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
                animator.Play("Jiang");
            else
                animator.SetTrigger("Jiang");
        }
    }
        private void OnTriggerEnter(Collider other)
    {
        if(other.tag=="Player")
        {
            Move.onBoat = true;
            dir = -player.transform.position + transform.position;
            ifboat = true;
        }
        if(other.tag=="End1")
        {
            Move.onBoat = false;
            ifboat = false;
        }
    }
}
