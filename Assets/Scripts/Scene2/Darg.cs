using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Darg : MonoBehaviour
{
    private bool isClicked = false;
    private bool ifClicked = false;
    private Transform currentTrans = null;
    private Vector3 oriMousePos;
    private Vector3 oriObjectScreenPos;

    public float rotateSpeed;
    private Rigidbody rb;

    
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100, LayerMask.GetMask("Draged")))//发射射线，返回布尔值的同时得到hit，即打到物体的信息。hit是raycasthit类型的
            {
                if (hit.transform == this.transform)
                {
                    currentTrans = hit.transform;
                    oriObjectScreenPos = Camera.main.WorldToScreenPoint(currentTrans.position);//将物体位置转化成屏幕坐标系坐标
                    oriMousePos = Input.mousePosition;//只在鼠标初次点击瞬间得到鼠标位置
                    ifClicked = true;
                    isClicked = !isClicked;//实现两次点击为一个循环的操作，简洁
                }
            }
            else
                ifClicked = false;
            
        }
        if (isClicked )
        {
         
            if (currentTrans != null&&ifClicked)//射线打到东西了
            {
                Vector3 curMousePos = Input.mousePosition;//时刻得到鼠标位置
                Vector3 mouseOffset = curMousePos - oriMousePos;
                Vector3 curObjectScreenPos = oriObjectScreenPos + mouseOffset;//类似简易相机跟随的逻辑，得到obj的实时屏幕坐标
                Vector3 curObjectWorldPos = Camera.main.ScreenToWorldPoint(curObjectScreenPos);//将屏幕坐标转换为世界坐标

                currentTrans.position = curObjectWorldPos;
                this.transform.position = new Vector3(currentTrans.position.x, currentTrans.position.y, GameObject.FindGameObjectWithTag("Player").transform.position.z );
                if (Input.GetKey(KeyCode.Q))
                    this.transform.Rotate(Vector3.forward * rotateSpeed * Time.deltaTime);
                if (Input.GetKey(KeyCode.E))
                    this.transform.Rotate(Vector3.back * rotateSpeed * Time.deltaTime);
            }
            
        }
        
    }
}
