using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour
{
    Vector3 fir;
    public Transform target;
    // Start is called before the first frame update
    void Start()
    {
        fir = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < target.position.y)
            transform.position += new Vector3(0, 1f, 0) * Time.fixedDeltaTime;
    }

}
