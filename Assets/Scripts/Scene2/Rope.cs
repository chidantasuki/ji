using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rope : MonoBehaviour
{
    public bool up;
    public GameObject target;
    Vector3 dir;
    public float speed;
    public bool start = false;
    void Start()
    {
        dir = target.transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (up&&start)
            if (transform.localScale.x > dir.x)
                transform.localScale -= new Vector3(1, 0, 0) * speed * Time.deltaTime;
        if (!up && start)
            if (transform.localScale.x < dir.x)
                transform.localScale += new Vector3(1, 0, 0) * speed * Time.deltaTime;


    }
}
