using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SimMove : MonoBehaviour
{
    private Rigidbody rb;
    public float moveSpeed;
    public float jumpForce;
    private Animator animator;
    int num = 0;
    public Text text;
    private ConstantForce cf;
    public Camera cm;
    public GameObject quad;
    public GameObject pl;
    public GameObject tg;
    public MonoBehaviour arrow;
    public GameObject yd1;
    public GameObject yd2;
    public GameObject yd3;
    public GameObject yd4;
    private bool rt = false;
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        cf = GetComponent<ConstantForce>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKey(KeyCode.A))
        //{
            transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
        //}
        /*if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.back * moveSpeed * Time.deltaTime);
        }*/
        
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
        }
        if (Input.GetKeyDown(KeyCode.Space) && num <= 1)
        {
            num += 1;
            animator.SetBool("Jump", true);
            //rb.velocity = new Vector3(rb.velocity.x, jumpForce * Time.fixedDeltaTime, rb.velocity.z);
            Invoke("ANIMCHANGE", 0.15f);
        }

        if(text.transform.localScale == new Vector3(0.1f, 0.1f, 1))
        {
            rt = true;
        }
        if (rt && text.transform.localScale.x <= 3.5f)
        {
            text.transform.localScale += new Vector3(0.01f, 0.01f, 0);
        }
    }

    void ANIMCHANGE()
    {
        rb.velocity = new Vector3(rb.velocity.x, jumpForce * Time.fixedDeltaTime, rb.velocity.z);
        //animator.SetBool("Jump", false);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Trigger")
        {
            num = 0;
            animator.SetBool("Jump", false);
        }
        if (collision.collider.tag == "End")
        {
            animator.SetTrigger("Die");
            //text.SetActive(true);
            
            text.transform.localScale += new Vector3(0.1f, 0.1f,0);
            //Time.timeScale = 0;
            moveSpeed = 0;
            
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Pass")
        {
            cf.force = new Vector3(0, 10, 0);
            rb.velocity = new Vector3(rb.velocity.x * 0.3f, jumpForce * Time.fixedDeltaTime * 0.6f, rb.velocity.z);
        }
        if (other.tag == "Pass2")
        {
            //cf.force = new Vector3(0, 10, 0);
            rb.velocity = new Vector3(rb.velocity.x * 0.1f, jumpForce * Time.fixedDeltaTime * 0.2f, rb.velocity.z);
            Invoke("Second2", 3.4f);
            Invoke("Second", 5.2f);
        }
        if(other.tag == "Pass3")
        {
            arrow.enabled = true;
        }
        if (other.tag == "Pass4")
        {
            yd1.AddComponent<Rigidbody>();
            yd2.AddComponent<Rigidbody>();
        }
        if (other.tag == "Pass5")
        {
            yd3.AddComponent<Rigidbody>();
            yd4.AddComponent<Rigidbody>();
        }
        if (other.tag == "PlayerEyes")
        {
            tg.AddComponent<Rigidbody>();
        }
    }

    void Second2()
    {
        quad.SetActive(false);
        pl.SetActive(false);
        cm.backgroundColor = new Color(0, 0, 0, 1);
    }
    void Second()
    {
        SceneManager.LoadScene("Second");
    }

}