using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{
    public float moveSpeed;

    void FixedUpdate()
    {
        gameObject.transform.position += new Vector3(-moveSpeed * Time.fixedDeltaTime,0,0) ;
    }
}
