using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SimpleMove : MonoBehaviour
{
    private Rigidbody rb;
    public float moveSpeed;
    public float jumpForce;
    public Collider s1;
    public Collider s2;
    public Collider s3;
    public GameObject text;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        
    }

    // Update is called once per frame
    void Update()
    {
       // ooo();
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back * moveSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.Space))
        {
            rb.velocity = new Vector3(rb.velocity.x, jumpForce * Time.fixedDeltaTime, rb.velocity.z);
        }
    }

    
    private void OnTriggerStay(Collider other)
    {
        if (other == s1)
        {
            text.SetActive(true);
            if(Input.GetKey(KeyCode.E))
                SceneManager.LoadScene("1");
        }
        if (other == s2)
        {
            text.SetActive(true);
            if (Input.GetKey(KeyCode.E))
                SceneManager.LoadScene("Second");
        }
        if (other == s3)
        {
            text.SetActive(true);
            if (Input.GetKey(KeyCode.E))
                SceneManager.LoadScene("Drop");
        }
    }
    private void OnTriggerExit(Collider other)
    {
        text.SetActive(false);
    }
    /*
    private void  ooo()
    {
        if (Input.GetKeyDown(KeyCode.E))
            SceneManager.LoadScene("1");
    }
    */
}
