﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class TreeScript : MonoBehaviour
{
    private Collider colliders;
    public Transform originalForm;
    public Transform cutForm;
    private Rigidbody pieceRb;
    public ParticleSystem smokeParticle;

    private void Start()
    {
        if (cutForm.gameObject.activeSelf)
            cutForm.gameObject.SetActive(false);
        colliders = GetComponent<Collider>();
        pieceRb = cutForm.GetChild(0).GetComponent<Rigidbody>();
    }
    public void Slash()
    {
        colliders.enabled = false;
        originalForm.gameObject.SetActive(false);
        cutForm.gameObject.SetActive(true);
        pieceRb.AddForce(Vector3.right * 3, ForceMode.Impulse);
        smokeParticle.Play();
    }
}
