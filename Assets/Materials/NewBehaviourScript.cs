using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.Universal;

public class NewBehaviourScript : MonoBehaviour
{
    public Slider slider;
    public Material mt;
    
    // Start is called before the first frame update
    void Start()
    {
        //mr = GetComponent<MeshRenderer>();
       // mt = GetComponent<Material>();
    }

    // Update is called once per frame
    void Update()
    {
        
        mt.SetFloat("_Float0", slider.value);
    }
}
